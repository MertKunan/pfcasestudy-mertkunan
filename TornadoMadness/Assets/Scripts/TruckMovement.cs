﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class TruckMovement : MonoBehaviour {

    private float currentRotation;
    public List<GameObject> wayPoints;
    public GameObject wayPointParent;
    public GameObject nextWaypoint;
    public int nextWaypointNumber;
    public float truckSpeed; // edit truck movement speed by changing this value from the editor
    public bool canMove;
    public bool isRotating;
    public GameObject mainCam;
    public Tween rotateTweenRight;
    public Tween rotateTweenLeft;
    public JoystickController JoystickController;


    //Gets the waypoints that are placed in the level for truck to follow 
    private void OnEnable()
    {
        wayPointParent = GameObject.FindGameObjectWithTag("WayPointParent");
        for (int i = 0; i < wayPointParent.transform.childCount; i++)
        {
            wayPoints.Add(wayPointParent.transform.GetChild(i).gameObject);
        }
        nextWaypointNumber = 0;
        nextWaypoint = wayPoints[nextWaypointNumber];
        mainCam = Camera.main.gameObject;
    }

    private void Start()
    {
        canMove = true;
    }

    //if the truck can move, changes the position of the truck according to the next waypoint it needs to go
    //if the truck succesfully reaches the next waypoint, it will rotate towards the next one keep on going
    private void Update()
    {
        if(canMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, nextWaypoint.transform.position, Time.fixedDeltaTime * truckSpeed);

            if (Vector3.SqrMagnitude(transform.position - nextWaypoint.transform.position) <= 0.01f)
            {
                if(nextWaypointNumber < wayPoints.Count-1)
                nextWaypointNumber++;
                nextWaypoint  = wayPoints[nextWaypointNumber];
                transform.DOLookAt(nextWaypoint.transform.position, 1f);
                Debug.Log("waypoint arrived, moving on to the next one");
            }
        }
        if(isRotating)
        {
            rotateTweenLeft.OnComplete(() =>
            {
                Debug.Log("LeftRotation Complete");
                isRotating = false;
                currentRotation -= 90;
            });
            rotateTweenRight.OnComplete(() =>
            {
                Debug.Log("Right Rotation Complete");
                currentRotation += 90;
                isRotating = false;
            });
        }
    }

    private void LateUpdate()
    {
       mainCam.transform.position = new Vector3(gameObject.transform.position.x, 10, gameObject.transform.position.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Obstacle" || collision.transform.tag == "Tornado")
        {
            Debug.Log("Game Over");
            SceneManager.LoadScene(0);
            canMove = false;
        }
        
    }

    // tweens the camera rotation before the truck rotates to the next waypoint for better user experince by using triggers placed with the waypoint prefabs
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Left" && !isRotating)
        {
            Debug.Log("turningLeft");
            isRotating = true;
            JoystickController.rotationType--;
            if (JoystickController.rotationType == 0)
                JoystickController.rotationType = 4;
            rotateTweenLeft = mainCam.transform.DORotate(new Vector3(50, currentRotation - 90, 0), 2);
        }
        else if (other.transform.tag == "Right" && !isRotating)
        {
            Debug.Log("Turning Right");
            JoystickController.rotationType++;
            if (JoystickController.rotationType == 6)
                JoystickController.rotationType = 2;
            isRotating = true;
            rotateTweenRight = mainCam.transform.DORotate(new Vector3(50, currentRotation+90, 0), 2);
        }
        else if (other.transform.tag == "FinishLine")
        {
            Debug.Log("LevelComplete");
            int currentLevel = PlayerPrefs.GetInt("currentLevel",0);
            if(currentLevel<2)
            {
                currentLevel++;
                PlayerPrefs.SetInt("currentLevel", currentLevel);
            }
            else
            {
                currentLevel = 0;
                PlayerPrefs.SetInt("currentLevel", currentLevel);
            }
            SceneManager.LoadScene(0);
            canMove = false;
        }
    }

}
