﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Can be found under Maincamera/Tornado/TornadoAOE
public class TornadaPush : MonoBehaviour
{
   
    public float pushForce;  //edit push force by changing this value from the editor
    public float tornadoDistancePower; // increase this value to lower the effect of distance of tornado

    //Summary
    //Detects if there is obstacle in the tornado range
    //Calculates the direction and the distance between the obstacle and the tornado
    //Adds pushing force to the obstacle according to the calculated direction and distance
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            Vector3 dir = other.transform.position - transform.position;
            dir = dir.normalized;
            float distance = Vector3.Distance(other.transform.position, transform.position);
            other.gameObject.GetComponent<Rigidbody>().AddForce(dir * pushForce * (tornadoDistancePower-distance));
            Destroy(other.gameObject, 3);
        }
    }
}
