﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public GameObject[] Levels;
    public int CurrentLevel;
    public TruckMovement Truck;
    public JoystickController JoystickController;
    public Canvas canvas;

    private void OnEnable()
    {
        CurrentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        SetUpLevel();
    }

    public void SetUpLevel()
    {
        Instantiate(Levels[CurrentLevel], Vector3.zero, Quaternion.identity);
    }

    public void StartLevel()
    {
        Truck.enabled = true;
        JoystickController.enabled = true;
        canvas.gameObject.SetActive(false);
    }
}
