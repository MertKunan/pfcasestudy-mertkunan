﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickController : MonoBehaviour {

    public Rigidbody tornadoRigidBody;
    public float dragPower; // change this value to make tornado movements faster or slower
    public bool isNormalRotation;
    public int rotationType;

    public Vector2 lastMousePos;

    private void Start()
    {
        Input.multiTouchEnabled = false;
        rotationType = 3;
    }

    //Detects the deltaposition of mouse/touchpoint each frame and uses it with given drag power to move tornado
    //The x and y positions are used according to the current rotation of the tornado
    void Update () {

        Vector2 deltaPos = Vector2.zero;

        if(Input.GetMouseButton(0))
        {
            //Improvement: Change to world point to prevent different speed on differen phone resolutions
            Vector2 currentMousePos = Input.mousePosition;

            if (lastMousePos == Vector2.zero)
            lastMousePos = currentMousePos;

            deltaPos = currentMousePos - lastMousePos;
            lastMousePos = currentMousePos;

            switch(rotationType)
            {
                case 3:
                    Vector3 typeZeroForce = new Vector3(deltaPos.x, 0, deltaPos.y) * dragPower;
                    tornadoRigidBody.AddForce(typeZeroForce);
                    break;
                case 2:
                    Vector3 typeOneForce = new Vector3(-deltaPos.y, 0, deltaPos.x) * dragPower;
                    tornadoRigidBody.AddForce(typeOneForce);
                    break;
                case 4:
                    Vector3 typeTwoForce = new Vector3(deltaPos.y, 0, -deltaPos.x) * dragPower;
                    tornadoRigidBody.AddForce(typeTwoForce);
                    break;
                case 1:
                    Vector3 typeThreeForce = new Vector3(-deltaPos.x, 0, -deltaPos.y) * dragPower;
                    tornadoRigidBody.AddForce(typeThreeForce);
                    break;
                case 5:
                    Vector3 typeFiveForce = new Vector3(-deltaPos.x, 0, -deltaPos.y) * dragPower;
                    tornadoRigidBody.AddForce(typeFiveForce);
                    break;
            }            
        }
        else
        {
            lastMousePos = Vector2.zero;
        }
	}

}
